const DataBase = require('./DB');
const path = require('path');
const fs = require('fs');
const FOLDER_TO_SEARCH = process.env.FOLDER_TO_SEARCH || './';

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function getDir(fullPath) {
  return path.dirname(fullPath).replace(/\\/g,"/").replace(new RegExp(`${FOLDER_TO_SEARCH.replace('.', '(\\.\\/)?')}\\/?`), '');
}

class FileModel {
  getFolderItems = pathName => {
    const result = {};

    const traverse = p => {
      try {
        const dirItems = fs.readdirSync(p);
        dirItems.forEach(item => {
          const fullPath = path.join(p, item);
          const expansion = item.substr(item.indexOf(".") + 1);
          try {
            const stats = fs.statSync(fullPath);
            if (stats.isDirectory()) {
              result[fullPath] = {
                name: item,
                dir: getDir(fullPath),
                size: bytesToSize(stats.size),
                birthtime: stats.birthtime.toDateString(),
                isFile: stats.isFile(),
                isDir: stats.isDirectory(),
              };
              // traverse(fullPath);
            } else {
              result[fullPath] = {
                name: item,
                dir: getDir(fullPath),
                exp: expansion,
                size: bytesToSize(stats.size),
                birthtime: stats.birthtime.toDateString(),
                isFile: stats.isFile(),
                isDir: stats.isDirectory(),
              };
            }
          } catch (err) {
            console.log(err);
          }
        });
      } catch (err) {
        console.log('ERROR', err);
      }
    }
    traverse(pathName);

    return result;
  };
}

module.exports = new FileModel();
