const { fileModel } = require('../models');
const FOLDER_TO_SEARCH = process.env.FOLDER_TO_SEARCH || './';

class fileController {

  getAllFiles(req, res) {
    const basePath = `${FOLDER_TO_SEARCH}/${req.query.path || ''}`;
    const folderItems = fileModel.getFolderItems(basePath);

    res.render('pages/home.hbs', {
      folderItems,
      pathForBack: req.query.path,
      token: req.cookies.token,
    });
  };
}

module.exports = new fileController();
