const userModel = require('../models/userModel');

class UserController {
  register(req, res) {
    const {
      method,
      body: { username, password },
    } = req;

    if (method === 'GET') return res.render('pages/create.hbs');

    userModel.register(username, password, result => {

      const { success, msg } = result;

      if (!success) {
        return res.render('pages/create.hbs', {
          erroMessage: msg,
        });
      } else {
        res.redirect('/user/login');
      }
    });
  }

  login(req, res) {
    const { username, password } = req.body;
    const { method } = req;

    if (method === 'GET') return res.render('pages/login.hbs', {
      token: req.cookies.token
    });

    userModel.loginPromised(username, password, result => {
      const { success, msg } = result;

      if (!success) {
        return res.render('pages/login.hbs', {
          errorMessage: msg,
        });
      }
      res.cookie('token', msg, {
        httpOnly: true,
      });
      res.redirect('/');
    });
  }

  logout(req, res) {
    res.clearCookie('token');
    res.redirect('/user/login');
  }

  isUserValid(req, res, next) {
    userModel.isValidToken(req.cookies.token || '', isValid => {
      const { url } = req;

      if (isValid) next();

      else if (url !== '/user/login' && url !== '/user/create') {
        res.redirect('/user/login');
      } else {
        next();
      }

      return isValid;
    });
  }
}

exports.UserController = new UserController();
