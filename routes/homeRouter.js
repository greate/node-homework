const homeRouter = require('express').Router();

const { fileController } = require('../controllers');
const { UserController } = require('../controllers/userController')

homeRouter.get('/', (req, res) => {

    // if (UserController.isUserValid(req, res, next)) {
        fileController.getAllFiles(req, res);
    // };
});

module.exports = homeRouter;
