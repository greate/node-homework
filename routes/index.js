const homeRouter = require('./homeRouter');
const fileRouter = require('./fileRouter');
const userRouter = require('./userRouter');

module.exports = {
  homeRouter,
  fileRouter,
  userRouter
};
