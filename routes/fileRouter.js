const fileRouter = require('express').Router();
const { fileController } = require('../controllers');

fileRouter.get('/', fileController.getAllFiles);

module.exports = fileRouter;
