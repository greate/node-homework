const express = require('express');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');
const fs = require("fs");
const url = require('url');

const { homeRouter, userRouter, fileRouter } = require('./routes');
const { userController } = require('./controllers/userController');

const app = express();

const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.set('views', __dirname + '/public/views');
app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/public/views/partials');

hbs.registerHelper('ifEqualsChained', function () {
  var options = arguments[arguments.length - 1];
  valueToTest = arguments[0];
  for (var i = 1; i < (arguments.length - 1); i++) {
    if (valueToTest === arguments[i]) {
      return options.fn(this);
    }
  }
  return options.inverse(this);
});

app.use(urlencodedParser);
app.use(cookieParser());
app.use(fileUpload({ createParentPath: true }));

app.use('/styles', express.static(__dirname + '/public/css'));
app.use(express.static(__dirname + '/public/images'));

app.use((req, res, next) => {
  // userController.isUserValid(req, res, next);

  next();
});

app.use('/files', fileRouter);
app.use('/user', userRouter);

app.use('/upload', (req, res) => {
  if (req.method === 'GET') return res.render('pages/upload.hbs');

  try {
    if (!req.files) {
      res.json({
        success: false,
        message: 'No file uploaded',
      });
    } else {
      let data = [];

      const filesList = Object.values(req.files);

      filesList.forEach((file, i) => {
        file.mv(__dirname + `/uploads/${i % 2 ? 'sub/' : ''}` + file.name, err => {
          const result = {
            name: file.name,
            mimetype: file.mimetype,
            size: file.size,
            status: true,
          };

          if (err) {
            result.status = false;
          }

          data.push(result);

          if (data.length === filesList.length) {
            res.json({
              success: true,
              message: 'File(s) are uploaded',
              data: data,
            });
          }
        });
      });

    }
  } catch (err) {
    res.status(500).json({ success: false, message: 'Server error' });
  }
  res.redirect('/');
});


app.get("/preview", (req, res) => {
  let filePath = /filepath=([^&]+)/.exec(req.url)[1];

  const queryObject = url.parse(req.url, true).query;


  if (filePath.indexOf("/") === 0) {
    filePath = filePath.substr(1, filePath.length);
  }

  fs.readFile(filePath, function (error, data) {
    if (error) {
      res.statusCode = 404;
      res.end("Resourse not found!");
    }
    else {
      res.end(data);
    }
  });
});

app.get("/download", (req, res) => {
  let filePath = /filepath=\/([^&]+)/.exec(req.url)[1];

  if (filePath.indexOf("/") === 0) {
    filePath = filePath.substr(1, filePath.length);
  }

  res.download(filePath);
});

app.get("/details", (req, res) => {
  const queryObject = url.parse(req.url, true).query;

  res.render('pages/fileDetails.hbs', {
    name: queryObject.name,
    size: queryObject.size,
    birthTime: queryObject.birth,
    filePath: queryObject.filepath.replace(/\\/g,"/")
  });
});

app.use('/', homeRouter);

app.listen(process.env.PORT || 3010, () => {
  console.log('Server started!');
});
